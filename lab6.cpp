/************************************
	* Mason Young
	* CPSC 1021, Section 003, F20
	* mmy@clemson.edu
	* Nushrat Humaira / Evan Hastings
*************************************/

#include <string>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <iomanip>

using namespace std;

typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;} //possibly need ;

int main(int argc, char const *argv[]) {
	// IMPLEMENT as instructed below
  	/*This is to seed the random generator */
  	srand(unsigned (time(0)));

  	/*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee inputEmployee[10]; //declaring the array of 10 employees
	//int i;
	for( int i = 0; i<10; i++) { //loops ten times for ten employee inputs
		std::cout << "Enter employee " << i+1 << "'s last name: " << endl;
		std::cin >> inputEmployee[i].lastName;//scanning for input of last name

		std::cout << "Enter employee " << i+1 << "'s first name: " <<  endl;
		std::cin >> inputEmployee[i].firstName;//scanning for input of first name

		std::cout << "Enter employee " << i+1 << "'s birth year: " <<  endl;
		std::cin >> inputEmployee[i].birthYear;//scanning for input of birth year

		std::cout << "Enter employee " << i+1 << "'s hourly wage: " <<  endl;
		std::cin >> inputEmployee[i].hourlyWage; //scanning for input of hourly wage
		std::cout << '\n'; //formatting lines

	}
		std::cout << '\n'; //formatting lines
	/*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
		random_shuffle(&inputEmployee[0],&inputEmployee[10], myrandom);
	/*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
	employee small[5]; //creating the smaller array of 5 employees

	for( int i =0; i < 5; i++)
	{
		small[i] = inputEmployee[i]; //gets the initial 5 employees to place into the small arr
	}
	/*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
	sort(&small[0],&small[5], name_order); //sorting the small array

	/*Now print the array below */
	for( employee i : small){ //traversing through the small array and printing
		std::cout << setw(20) << right << i.lastName+","+i.firstName <<endl; //prints the last name starting on the right
		std::cout << setw(20) << right << i.birthYear << endl; //prints the birthYear after last Name
		std::cout << setw(20) << right << fixed << setprecision(2)<<i.hourlyWage<<endl; //prints the hourly wage fixed to the right after birthYear
		std::cout <<'\n'; //formatting lines
	}
	return 0;
}



	/*This function will be passed to the sort funtion. Hints on how to implement
	* this is in the specifications document.*/
	bool name_order(const employee& lhs, const employee& rhs) {
		//IMPLEMENT
		if (rhs.lastName > lhs.lastName){
			return true; //makes sure the order of names is sorted by lastName
		}
		else{
			return false;// if lhs < rhs for last names, then return false. This will help the sort function know whether or not to sort the element
		}
	}
